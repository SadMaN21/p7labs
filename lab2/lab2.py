import matplotlib.pyplot as plt
import numpy as np


class Biome(object):
    def __init__(self, pred_init=1, prey_init=1, tmax=10, dt=0.001):
        self._dt = dt
        self._n_iters = int(tmax / dt)
        # Intial state
        self.pred_init = pred_init
        self.prey_init = prey_init
        # Data arrays
        self.time = None
        self.prey = None
        self.pred = None

    def run(self, alpha, beta, delta, gamma):
        """
        Simulation settings:
            alpha - prey growth
            beta - prey mortality
            delta - predator growrh
            gamma - predator mortality
        """
        self.time = np.zeros(self._n_iters)
        self.prey = np.zeros(self._n_iters)
        self.pred = np.zeros(self._n_iters)
        self.prey[0] = self.prey_init
        self.pred[0] = self.pred_init

        # Lotka-Volterra equations
        for i in range(self._n_iters - 1):
            self.prey[i + 1] = self.prey[i] + self._dt * self.prey[i] * (alpha - self.pred[i] * beta)
            self.pred[i + 1] = self.pred[i] + self._dt * self.pred[i] * (delta * self.prey[i] - gamma)

        self.time[1:] = np.add.accumulate([self._dt] * (self._n_iters - 1))

        return self.time, self.prey, self.pred

    def plot(self):
        _, axes = plt.subplots(1, 1, figsize=(9, 7), dpi=200)
        plt.xlabel('time')
        plt.ylabel('n')
        axes.plot(self.time, self.pred, label='predators', color='r')
        axes.plot(self.time, self.prey, label='preys', color='b')
        axes.legend()
        axes.grid()

        plt.show()

    def plot_cycle(self):
        _, axes = plt.subplots(1, 1, figsize=(9, 7), dpi=200)
        plt.xlabel('n predators')
        plt.ylabel('n preys')
        axes.plot(self.pred, self.prey, color='black')
        axes.grid()

        plt.show()


def main():
    biome = Biome(tmax=10)
    biome.run(2, 5, 2, 2)
    biome.plot()
    biome.plot_cycle()


if __name__ == "__main__":
    main()
